Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: hub
Source: https://github.com/github/hub
Files-Excluded: vendor

Files: *
Copyright: 2009 Chris Wanstrath
           2013 Jingwen Owen Ou
License: Expat
Comment:
 Hub was created by GitHub co-founder and former CEO Chris Wanstrath
 (@defunkt) in 2009.  It began life at https://github.com/defunkt/hub
 as a Ruby program, @defunkt remained active in its development until 2011.
 It remained as a Ruby program v1.12.4, released on 2014-12-25.
 .
 Even though Hub has had many contributions from many other people
 since then, and GitHub staff Mislav Marohnic (@mislav) has been the
 main maintainer of Hub since 2010 to this day, I think it is fair
 to keep the copyright owner as-is to honour its original creator.
 .
 The current package, the v2 series, was re-written in Go
 as initiated by Jingwen Owen Ou <jingweno@gmail.com> (@jingweno)
 in April 2013.  It began life as "gh" at https://github.com/jingweno/gh,
 and was adopted and merged into "Hub" in 2014,
 see https://github.com/github/hub/issues/475.
 .
 https://github.com/jingweno/gh contains a LICENSE.md file which
 says "Copyright (c) 2013 Jingwen Owen Ou" (MIT/Expat license),
 though that information wasn't merged into the LICENSE file in
 https://github.com/github/hub.
 .
 So, yes, it does seem a little bit complicated, probably prompting
 the Hub maintainers to the URL https://github.com/github/hub/contributors
 under Authors in README.md.

Files: debian/*
Copyright: 2016 Tianon Gravi <tianon@debian.org>
           2018 Anthony Fok <foka@debian.org>
License: Expat

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
